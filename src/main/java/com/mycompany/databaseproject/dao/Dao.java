/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author Admin
 */
public interface Dao<T> {
    T get(int id);
    List<T> getALL();
    T save (T obj);
    T update (T obj);
    int delete (T obj);
    List<User> getALL (String where,String order);
}
